﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ExamWADv2.Models;

namespace ExamWADv2.Models
{
    public class ExamWADv2Context : DbContext
    {
        public ExamWADv2Context (DbContextOptions<ExamWADv2Context> options)
            : base(options)
        {
        }

        public DbSet<ExamWADv2.Models.Category> Category { get; set; }

        public DbSet<ExamWADv2.Models.Product> Product { get; set; }
    }
}
